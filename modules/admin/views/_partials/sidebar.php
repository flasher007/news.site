<?
use yii\bootstrap\Nav;

$user = \Yii::$app->user;
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Администрирование</h3>
    </div>
    <div class="panel-body">
        <?
        $items = [
            [
                'label' => 'Новости',
                'url'   => ['/admin/news']
            ],
            [
                'label' => 'Пользователи',
                'url'   => ['/admin/user'],
            ],
            [
                'label' => Yii::t('user', 'Роли'),
                'url' => ['/rbac/role/index'],
                'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
            ],
            [
                'label' => Yii::t('user', 'Доступы'),
                'url' => ['/rbac/permission/index'],
                'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
            ],
            [
                'label' => \Yii::t('user', 'Правила'),
                'url'   => ['/rbac/rule/index'],
                'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
            ],
        ];
        echo Nav::widget([
            'options' => ['class' => 'nav nav-pills nav-stacked'],
//            'submenuTemplate' => '<ul role="menu" class="sub-menu">{items}</ul>'."\n",
            'items'   => $items,
        ]);
        ?>
    </div>
</div>