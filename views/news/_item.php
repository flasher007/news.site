<?php
/**
 * @var $news \app\models\News
 */
?>
<div class="news_item">
    <h4 class="list-group-item-heading">
        <?php if (Yii::$app->user->isGuest) { ?><?=$news->title?>
        <?} else {?>
            <?php echo \yii\helpers\Html::a($news->title, ['news/view', 'id' => $news->id])?>
        <?}?>
    </h4>
    <p class="list-group-item-text"><?=$news->preview?></p>
</div>