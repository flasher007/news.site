<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-9">
        <div class="news-index">

            <h1><?= Html::encode($this->title) ?></h1>
            <?php
            foreach ($dataProvider->models as $model) {
                echo $this->render('_item', [
                    'news' => $model
                ]);
            }

            echo LinkPager::widget([
                'pagination' => $dataProvider->getPagination(),
            ]);
            ?>

        </div>
    </div>
</div>
