<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => 'Новости',
                'brandUrl'   => Yii::$app->homeUrl,
                'options'    => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            $user            = Yii::$app->user;
            $authManager     = Yii::$app->authManager;
            $isGuest         = $user->isGuest;
            $isAdmin         = (bool) $authManager->getAssignment('admin', $user->getId());
            $isAuthorizeUser = (bool) $isAdmin || $authManager->getAssignment(\app\models\User::DEFAULT_ROLE, $user->getId());

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items'   => [
                    ['label' => 'Главная', 'url' => ['/site/index']],
                    ['label' => 'Новости', 'url' => ['/news']],
                    ['label' => 'Администрирование', 'url' => ['/admin'], 'visible' => $isAdmin],
                    ['label' => 'Вход', 'url' => ['/user/security/login'], 'visible' => $isGuest],
                    ['label'   => "<span class=\"glyphicon glyphicon-user\"></span> " . $user->identity->username,
                     'url'     => ['/user/settings'],
                     'encode'  => false,
                     'visible' => $isAuthorizeUser],
                    ['label'       => 'Выход',
                     'url'         => ['/site/logout'],
                     'linkOptions' => ['data-method' => 'post'],
                     'visible'     => $isAuthorizeUser],
                ],
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; Новости <?= date('Y') ?></p>

            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
