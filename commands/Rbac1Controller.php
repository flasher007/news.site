<?php

namespace app\commands;

use Yii;

class Rbac1Controller extends \yii\console\Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'Create a post';
        $auth->add($createPost);

        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'Update post';
        $auth->add($updatePost);

        $viewFullPost = $auth->createPermission('viewFullPost');
        $viewFullPost->description = 'View full post';
        $auth->add($viewFullPost);

        $manageUsers = $auth->createPermission('manageUsers');
        $manageUsers->description = 'Manage users';
        $auth->add($manageUsers);

        $regUser = $auth->createRole('reguser');
        $auth->add($regUser);
        $auth->addChild($regUser, $viewFullPost);

        $moderator = $auth->createRole('moderator');
        $auth->add($moderator);
        $auth->addChild($moderator, $createPost);
        $auth->addChild($moderator, $updatePost);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $moderator);
        $auth->addChild($admin, $manageUsers);

        $auth->assign($auth->getRole('admin'), 1);
    }


}
