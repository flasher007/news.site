<?php

use yii\db\Migration;

class m170209_153638_insert_data extends Migration
{
    public function up()
    {
        $this->insert('users',[
            'id'=>1,
            'name'=>'Admin',
            'email'=>'tulov.alex@gmail.com',
            'hash_password'=>'9cb4fc9551b1315a96405fdf6f36b882c04af546',
        ]);
    }

    public function down()
    {
        echo "m170209_153638_insert_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
