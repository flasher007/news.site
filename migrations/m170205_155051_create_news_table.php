<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m170205_155051_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id'         => $this->primaryKey(),
            'author_id'  => $this->integer()->notNull(),
            'date'       => $this->date()->notNull(),
            'title'      => $this->string(255)->notNull(),
            'preview'    => $this->text(),
            'detail'     => $this->text(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'is_active'  => $this->integer(1)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
