<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $author_id
 * @property string  $date
 * @property string  $title
 * @property string  $preview
 * @property string  $detail
 * @property string  $created_at
 * @property string  $updated_at
 * @property integer $is_active
 */
class News extends \yii\db\ActiveRecord
{
    const DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['author_id', 'is_active'], 'integer'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['preview', 'detail'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'author_id'  => 'Автор',
            'date'       => 'Дата',
            'title'      => 'Заголовок',
            'preview'    => 'Анонс',
            'detail'     => 'Текст',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'is_active'  => 'Активность',
        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            if (!$this->id) {
                $this->created_at = date(static::DATE_FORMAT);
                $this->updated_at = date(static::DATE_FORMAT);
                $this->author_id  = Yii::$app->user->getId();
            } else {
                $this->updated_at = date(static::DATE_FORMAT);
            }
        }

        return parent::beforeSave($insert);
    }


}
