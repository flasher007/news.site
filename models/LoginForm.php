<?php

namespace app\models;

use Yii;

/**
 * Class LoginForm
 * @package app\models
 */
class LoginForm extends \dektrium\user\models\LoginForm
{
    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'login'      => Yii::t('user', 'Логин'),
            'password'   => Yii::t('user', 'Пароль'),
            'rememberMe' => Yii::t('user', 'Запомнить'),
        ];
    }
}
