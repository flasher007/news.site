<?php

namespace app\models;

use Yii;


class User extends \dektrium\user\models\User
{
    const DEFAULT_ROLE = 'reguser';
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username'          => \Yii::t('user', 'Логин'),
            'email'             => \Yii::t('user', 'Email'),
            'registration_ip'   => \Yii::t('user', 'Registration ip'),
            'unconfirmed_email' => \Yii::t('user', 'New email'),
            'password'          => \Yii::t('user', 'Пароль'),
            'created_at'        => \Yii::t('user', 'Registration time'),
            'last_login_at'     => \Yii::t('user', 'Last login'),
            'confirmed_at'      => \Yii::t('user', 'Confirmation time'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        $auth   = Yii::$app->authManager;
        $editor = $auth->getRole(self::DEFAULT_ROLE);
        $auth->assign($editor, $this->id);

        parent::afterSave($insert, $changedAttributes);
    }
}
